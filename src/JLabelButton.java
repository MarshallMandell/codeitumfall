import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class JLabelButton {

    public static void main(String[] args) {
        new JLabelButton();
    }

    private JPanel panel;

    public JLabelButton() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                //Declaring the buttons, panels, etc...
                JButton button = new JButton("Click");

                panel = new JPanel();
                panel.add(button);

                final JFrame frame = new JFrame("Button Pressed");
                frame.setSize(1000, 1000);
                frame.setVisible(true);
                frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
                frame.add(panel);

                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        panel.add(new JLabel("You clicked me"));
                        panel.revalidate();
                    }
                });
            }
        });
    }
}