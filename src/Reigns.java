import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Reigns extends JFrame {

	public static void main(String[] args) throws IOException {
		String Board = "img/ReignsBoard.jpg";
		File file = new File(Board);
        BufferedImage BoardIM = ImageIO.read(file);
        JLabel label = new JLabel(new ImageIcon(BoardIM));
        JFrame f = new JFrame();
        f.setLayout(new GridLayout(3,4));
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(label);
        f.pack();
        f.setLocation(100,100);
        f.setVisible(true);

	}
	
}
