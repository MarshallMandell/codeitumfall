import java.io.IOException;
import java.applet.*;
import java.net.*;
public class Conversation {

	public static void main(String[] args) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec("osascript -e 'set volume 10'");
		Process pr1 = rt.exec("say -v Fred I am Batman");
		pr1.waitFor();
		Process pr2 = rt.exec("say -v Agnes No your not Fred");
		pr2.waitFor();
		Process pr3 = rt.exec("say -v Fred What are you talking about?");
		pr3.waitFor();
		Process pr4 = rt.exec("say -v Agnes  your not Batman");
		pr4.waitFor();
		Process pr5 = rt.exec("say -v Fred Yes I am.");
		pr5.waitFor();
		Process pr6 = rt.exec("say -v Agnes No your not");
		pr6.waitFor();
		Process pr7 = rt.exec("say -v Fred Am to");
		pr7.waitFor();
		
	}

}
