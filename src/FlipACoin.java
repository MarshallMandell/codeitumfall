import java.util.Scanner;

public class FlipACoin {

	private static Scanner console_Scanner;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	while(true){
		console_Scanner = new Scanner (System.in);
		System.out.println("+---=Flip A Coin=---+");
		System.out.println("Do you confirm Y/N?");
		int RandomNumber = (int) (1+(Math.random()*2));
		String Confirm = console_Scanner.next();
		String HeadsorTails = null;
		if (RandomNumber == 1){
			HeadsorTails = "Heads";
		}
		if (RandomNumber == 2){
			HeadsorTails = "Tails";
		}
		if (Confirm.compareTo("Y") == 0){
			System.out.println("Its " + HeadsorTails + "!");
			return;
		}
		if (Confirm.compareTo("N") == 0){
			return;
		}
	}
	}

}
//System.out.println("The random number is " + HeadsorTails + "!");