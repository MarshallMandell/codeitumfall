import java.util.Scanner;

public class RandomNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	while(true){
		System.out.println("           ");
		System.out.println("           ");
		System.out.println("           ");
		System.out.println("         ------_+=======+_------         ");
		System.out.println("Enter (a) - (b) and it will pick a random number!");
		Scanner console_Scanner = new Scanner (System.in);
		System.out.println("Enter your (a) / first number.");
		
		int Aval = console_Scanner.nextInt();
		System.out.println("Enter your (b) / second number.");
		int Bval = console_Scanner.nextInt();
		System.out.println("Do you confirm Y/N?");
		String Confirm = console_Scanner.next();
		int RandomNumber = (int) (Aval+(Math.random()*Bval));
		if (Confirm.compareTo("Y") == 0||Confirm.compareTo("y")==0){
			System.out.println("The random number is " + RandomNumber + "!");
		}
		if (Confirm.compareTo("N") == 0||Confirm.compareTo("n")==0){
			continue;
		}
		break;
		}
	}

}
